package com.rueggerllc.services.hello;

import javax.jws.WebService;

import org.apache.log4j.Logger;

@WebService(endpointInterface = "com.rueggerllc.services.hello.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	
	private static Logger logger = Logger.getLogger(HelloWorldImpl.class);
	
	public String getHelloWorldAsString() {
		logger.info("YO WE ARE IN THE HELLO WORLD IMPL CLASS!");
		return "Hello World JAX-WS";
	}
	

}
